import { NAVIGATION } from "../action-types/navigation.actionTypes";

export const navigationAction = (tabClicked) => dispatch => {
    dispatch({
     type: NAVIGATION.SWITCH_TAB,
     payload: tabClicked,
    })
   }