import React, { Component } from 'react';
import { connect } from 'react-redux';
import { apiRequestInitialAction, apiRequestOnUpdateAction } from './actions/api.action';
import { initializeSocket } from './api';
import './App.css';
import GameInfo from './components/GameInfo';
import Header from './components/Header';
import Navigation from './components/Navigation';



const globalStyling = {
    backgroundColor: 'white',
    height: '100vh'
}
export class App extends Component {
    componentDidMount = () => {
        initializeSocket(
            initialData => {this.props.onConnect(initialData)},
            updateData => {this.props.onUpdate(updateData)},
            );
    }

    render() {
        return (
            <div style={globalStyling}>
                <header>
                    <Header/>
                </header>
                <nav>
                    <Navigation/>
                </nav>
                <section>
                    <div>
                        <GameInfo/>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = dispatch => ({
    onConnect: (incomingData) => dispatch(apiRequestInitialAction(incomingData)),
    onUpdate: (incomingData) => dispatch(apiRequestOnUpdateAction(incomingData)),
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
