import io from 'socket.io-client';

const socket = io('192.168.1.47:3000');

const initializeSocket = (onConnect, onUpdate) => {
    socket.on('connect', event => onConnect(event));
    socket.on('update', event => onUpdate(event));
}

export { initializeSocket, };

