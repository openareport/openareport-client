import { API } from "../action-types/api.actionTypes";

const initialState = {
    server: {},
    global: {
        players: [
            {
                name: 'Greg',
                id: 0,
                skin: 'bullshit',
                score: ['Greg', 'Greg'],
            }
        ],
    },
    current: {
        players: [
            {
                name: 'Greg',
                id: 0,
                skin: 'bullshit',
                score: ['Greg',],
            }
        ],
    }

}

export default (state = initialState, { type, payload }) => {
    console.log(state, payload )
    if(payload) {
        try {
            payload = JSON.stringify(payload)
        } catch (e) {
            console.error('Input object could not be parsed')
            return;
        }
    }
    switch (type) {
    case API.SOCKET_REQUEST:
        return {
            ...state,
            current: payload,
        }
    case API.INITIAL_REQUEST:
        return {
            ...state,
            global: payload,
        }

    default:
        return state
    }
}

export const getAllPlayers = (currentOrGlobal, state) => {
    console.log(state)
    switch (currentOrGlobal) {
        case 'current':
            return state.api.current.players;
        default:
        case 'global':
            return state.api.global.players;

    }
}
