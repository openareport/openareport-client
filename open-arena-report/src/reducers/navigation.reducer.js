import { NAVIGATION } from "../action-types/navigation.actionTypes";

const initialState = {
    navigation: 'current'
}

export default (state = initialState, {type, payload}) => {
  switch (type) {
      case NAVIGATION.SWITCH_TAB:
      return {
        ...state,
        navigation: payload
      };
    default:
      return state;
  }
};
