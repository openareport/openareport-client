import { combineReducers } from 'redux';
import apiReducer from './api.reducer';
import navigationReducer from './navigation.reducer';

export default combineReducers({
    navigation: navigationReducer,
    api: apiReducer,
});