import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllPlayers } from '../reducers/api.reducer';

export class Players extends Component {
    render() {
        console.log(this.props)
        return (
            <div>
                <h2>
                    Players info
                </h2>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        players: getAllPlayers(state.navigation.navigation, state)
    }

}

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Players)
