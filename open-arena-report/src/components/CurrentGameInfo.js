import React, { Component } from 'react';
import { connect } from 'react-redux';
import Players from './Players';

export class CurrentGameInfo extends Component {

    render() {
        return (
            <div>
                <Players/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrentGameInfo)
