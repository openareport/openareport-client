import React, { Component } from 'react';
import { connect } from 'react-redux';
import { navigationAction } from '../actions/navigation.action';

class Navigation extends Component {
    headerStyling = {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }

    itemStyling = {
        border: '1px solid black',
        width: '100%',
        textAlign: 'center',
        cursor: 'pointer',
    }

    selectedItemStyling = (item) => {
        return {color: (this.props.navigation.navigation === item? 'red': 'blue')}
    }
    render() {
        return (
            <div style={this.headerStyling}>
                <div style={{...this.itemStyling, ...this.selectedItemStyling('leaderboard')}} onClick={() => this.props.onTabClicked('leaderboard')}>Leaderboard</div>
                <div style={{...this.itemStyling, ...this.selectedItemStyling('current')}} onClick={() => this.props.onTabClicked('current')}>Current game</div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    onTabClicked: (tab) => dispatch(navigationAction(tab))
})

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);